const express = require('express');
const router = express.Router();

const userController = require('../../controllers/user.controller');

// get all data
router.get('/', userController.list);
// get data using id
router.get('/:id', userController.show);
// post data
router.post('/register', userController.create);
// update data using id
router.put('/update/:id', userController.update);
// delete data using id
router.delete('/:id', userController.delete);
// create token and login
router.post("/login", userController.login);

module.exports = router;