const { User } = require("../models/user.model");
const bcrypt = require("bcryptjs");
const secret = process.env.SECRET || "the default secret";
const jwt = require("jsonwebtoken");

// service for get all  data
exports.list = async function (res) {
    try {
        const user = await User.find({});
        return user;
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};


// service for get data using id
exports.show = async function (id, res) {
    try {
        const user = await User.findById(id);
        return user;
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
// service for post data
exports.create = async function (user, res) {
    try {
        const dat = await new User(user);
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(dat.password, salt);
        dat.password = hash;
        await dat.save();
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};
// service for update data using id
exports.update = async function (id, user, res) {
    try {
        const dat = await User.findByIdAndUpdate(id, user, { new: true });
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};
// service for delete data using id
exports.delete = async function (id, res) {
    try {
        const dat = await User.findByIdAndRemove(id);
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};

// service for check email is present in db or is a valid user for login
exports.logincheck = async function (email, res) {
    try {
        err = { status: "error", message: "email no is not registered" }
        check = await User.findOne({ email: email });
        if (!check) throw err;
    } catch (e) {
        throw err;
    }
};

// service for  admin login
exports.login = async function (user, res) {
     email = user.email;
    const check = await User.findOne({ email: email });
    password = check.password;
    try {
        isMatch = await bcrypt.compare(user.password, password);
        if (isMatch == false) throw Error('invalid password');
        const payload = { id: check.id, username: check.username };
        const token = jwt.sign(payload, secret, { expiresIn: 36000 });
        return {

            username: check.username,
            user_authentication: "sucess",
            token,
        };
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
