const Joi = require('@hapi/joi');
const createValidation = data => {
    const schema = Joi.object({
        name: Joi.string().min(4).optional(),
        password: Joi.string().min(4).required(),
        email: Joi.string().email().required(),

    });
    return schema.validate(data);
};
const updateValidation = data => {
    const schema = Joi.object({
        name: Joi.string().min(4).optional(),
        password: Joi.string().min(4).optional(),
        email: Joi.string().email().optional(),
    });
    return schema.validate(data);
};
module.exports.createValidation = createValidation;
module.exports.updateValidation = updateValidation;
