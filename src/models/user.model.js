const mongoose = require("mongoose");

const User = mongoose.model("user", {
  name: { type: String, required: false },
  email: { type: String, required: true },
  password: { type: String, required: true },
});

module.exports = { User };
